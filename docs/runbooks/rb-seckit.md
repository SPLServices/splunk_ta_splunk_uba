# Using OOB Configuration

## Requirements

* Supported Operating System
* Splunk Enterprise or Splunk Cloud

## Splunk Cloud

1. Open a TA deployment request for `Splunk_TA_Splunk_UBA `
2. Utilize Services or Admin on Demand to patch TA
3. Define each index [indexes.conf](../../src/SecKit_splunk_index_2_Splunk_UBA_home/default)

## Splunk Enterprise Cluster Master

1. Download latest `Splunk_TA_Splunk_UBA-*_indexers.tar.gz`
2. Splunk Cluster Master unzip to master-apps
3. Deploy one of
	4. `SecKit_splunk_index_2_Splunk_UBA_home-<latest>.tar.gz`
	5. `SecKit_splunk_index_2_Splunk_UBA_vol-<latest>.tar.gz`

## Splunk Enterprise Indexers and Intemediate Forwarders

1. Download latest `Splunk_TA_Splunk_UBA-*_indexers.tar.gz`
2. Login (sudo) to the splunk user `sudo su - splunk`
3. Install the app `splunk app install <filename>`
4. Define custom indexes as neded
4. Restart splunk `systemctl restart splunk`
5. Deploy one of
	4. `Splunk_TA_Splunk_UBA-<latest>.tar.gz`
	5. `Splunk_TA_Splunk_UBA<latest>-<latest>.tar.gz`

## Splunk Enterprise Search Head

1. Download latest `Splunk_TA_Splunk_UBA-<latest>-_search_heads.tar.gz`
2. Install the app `splunk app install <filename>`

## Source Operating System Configuration

### General

Pre-implementation Procedure

1. Verify the splunk_uba instance utilize the default log location `/var/log/splunk_uba/splunk_uba*.log*`

## Splunk Deployment Server

The following steps will onboard all splunk_uba instances use a common index consider customer requirements and clone + tailor as needed.

1. Download and Deploy `Splunk_TA_Splunk_UBA_DeploymentServer-<latest>.tar.gz`
2. Download and unzip to deployment-apps
	3. `Splunk_TA_Splunk_UBA-*.tar.gz`
	4. `Splunk_TA_Splunk_UBA_4_inputs*.tar.gz`
3. Add the following stanzas to `%SPLUNK_HOME/etc/apps/Splunk_TA_Splunk_UBA_DeploymentServer/local/serverclass.conf` define a while list per environment requirements

	```
	[serverClass:seckit_all_4_Splunk_UBA]
	machineTypesFilter = linux-*
	whitelist.0 =
	```


## Post Install Validation

1. Run the following search or derivative based on deployment scope

```| tstats count where index=appsplunk_uba* by host```
