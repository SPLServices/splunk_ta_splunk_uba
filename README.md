Repository for field created addon for splunk_uba

## Support
Updates/Modifications are developed in the same way customers can individually modify or create any add on. Support is limited the functionality creation and modification of the TA but excludes the add on content.

Find a bug, got a suggestion please use the project issue tracker, offer a pull request or upload a diff.

## Guidance Contained Limitation of Warranty
Guidance in the form of configuration choices provided are the best effort of contributors based on their experiences and is expected to be the best appropriate for many organizations. Specific requirements should be evaluated and addressed this is simply offered as a "good place to start"

##  Documentation

[Start here](docs/README.md)

## License

All original files licensed Apache 2.0
