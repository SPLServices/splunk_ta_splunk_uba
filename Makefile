-include common.mk

.PHONY=help package clean
.DEFAULT=help

OPTIONAL_GIT_DEPS=

APPS_DIR=src
OUT_DIR=out
BUILD_DIR=out/build
PACKAGES_DIR=$(OUT_DIR)/work/packages
STANDALONE_BUILD=$(OUT_DIR)/work/standalone
PARTITIONED_DIR=$(OUT_DIR)/release/partitioned
STANDALONE_DIR=$(OUT_DIR)/release/splunkbase
OPTIONAL_DEPENDENCY_DIR=$(OUT_DIR)/work/optional_dependencies
TEST_RESULTS=test-reports


MAIN_APP ?= $(shell ls -1 $(APPS_DIR))
VERSION=$(shell crudini --get src/$(MAIN_APP)/default/app.conf launcher version)
BUILD_NUMBER?=0000
COMMIT_ID ?=$(shell git rev-parse --short HEAD)
BITBUCKET_BRANCH?=local

APP_SOURCE_DIRS=$(foreach app,$(DEPENDENCY_APPS),$(APPS_DIR)/$(app))

STANDALONE_DEP_DIR=$(STANDALONE_BUILD)/$(MAIN_APP)/appserver/addons

PACKAGE_SLUG=D$(COMMIT_ID)
ifneq (,$(findstring master, $(BITBUCKET_BRANCH) ))
	PACKAGE_SLUG=S$(COMMIT_ID)
endif
ifneq (,$(findstring release, $(BITBUCKET_BRANCH) ))
	PACKAGE_SLUG=R$(COMMIT_ID)
endif

help: ## Show this help message.
	@echo 'usage: make [target] ...'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' $(MAKEFILE_LIST) | column -t -c 2 -s ':#' | sed 's/^/  /'

clean: ## Remove artifacts
	@rm -rf $(OUT_DIR)

$(PACKAGES_DIR):
	@mkdir -p $(PACKAGES_DIR)

$(PARTITIONED_DIR):
	@mkdir -p $(PARTITIONED_DIR)

$(STANDALONE_BUILD):
	@mkdir -p $(STANDALONE_BUILD)

$(STANDALONE_DIR):
	@mkdir -p $(STANDALONE_DIR)


package: ## Package each app
package: clean
	@mkdir -p $(OUT_DIR)
	@mkdir -p $(BUILD_DIR)
	@mkdir -p $(PACKAGES_DIR)
	@mkdir -p $(PARTITIONED_DIR)

	@for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do \
		cp -R src/$$i $(BUILD_DIR) ;\
		crudini --set $(BUILD_DIR)/$$i/default/app.conf launcher version $(VERSION)$(PACKAGE_SLUG);\
		crudini --set $(BUILD_DIR)/$$i/default/app.conf install build $(BUILD_NUMBER);\
		slim generate-manifest --update -o $(OUT_DIR)/app.manifest $(BUILD_DIR)/$$i ;\
		cp $(OUT_DIR)/app.manifest $(BUILD_DIR)/$$i/app.manifest ;\
	done

	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do find $(BUILD_DIR)/$$i  -type d -exec bash -c "chmod o-w,g-w,a+X '{}'" \; ; done
	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do find $(BUILD_DIR)/$$i  -type f -exec bash -c "chmod o-w,g-w,a-x '{}'" \; ; done
	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do if [ ! -d $(BUILD_DIR)/$$i/bin ]; then break; fi ; chmod u+x,g+x $(BUILD_DIR)/$$i/bin/* ; done
	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do slim package -o $(STANDALONE_DIR) $(BUILD_DIR)/$$i ; done

	slim partition -o $(PARTITIONED_DIR) $(STANDALONE_DIR)/$(MAIN_APP)-$(VERSION)-$(PACKAGE_SLUG).tar.gz

package_test: ## Package Test
package_test: package
	@mkdir -p test-reports
	splunk-appinspect inspect $(STANDALONE_DIR)/$(MAIN_APP)-$(VERSION)-$(PACKAGE_SLUG).tar.gz --data-format junitxml --output-file test-reports/precert-cloud.xml --mode precert --included-tags cloud
	splunk-appinspect inspect $(STANDALONE_DIR)/$(MAIN_APP)-$(VERSION)-$(PACKAGE_SLUG).tar.gz --data-format junitxml --output-file test-reports/precert.xml --mode precert

initappmanifest: ## Initialize a basic app.conf and app.manifest
initappmanifest:
	@for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do \
		if [ ! -e src/$$i/app.manifest ]; then \
			slim generate-manifest -o src/$$i/app.manifest src/$$i;\
		fi \
	done


devlink: ## Link apps into a Splunk installation
devlink:
	@if [ -d $(SPLUNK_HOME)/etc/apps ]; then \
		for i in $(realpath $(APP_SOURCE_DIRS)); do \
			ln -s $$i $(SPLUNK_HOME)/etc/apps; \
		done \
	else \
		echo "Could not find Splunk app home at $(SPLUNK_HOME)/etc/apps"; \
		exit 1; \
	fi

run_in_docker: ## Build a docker image and run it with the app installed
run_in_docker: optional_dependencies
	@echo "Building image"
	$(eval $@_IMAGE := $(shell docker build -q .))
	@echo "Starting container"
	$(eval $@_CONTAINER := $(shell docker run --env SPLUNK_START_ARGS="--accept-license" -p 8000:8000 -d --rm $($@_IMAGE)))
	$(eval $@_OS := $(shell uname))
	@echo "Launching Splunk "
	@sleep 10
	@echo "Launching browser"
	@case $($@_OS) in \
		"Darwin") open http://localhost:8000 ;; \
		*) which xdg-open > /dev/null && xdg-open http://localhost:8000 || echo "Splunk is ready at http://localhost:8000" ;; \
	esac
	@echo "Press Ctrl-C to stop and remove the container"
	@docker attach $($@_CONTAINER) || echo "Done."
